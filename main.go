package main

import (
	"encoding/csv"
	"encoding/json"
	"encoding/xml"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

type result struct {
	Result     string `json:"concatAB,omitempty" xml:"concatAB,omitempty"`
	Err        string `json:"error,omitempty" xml:"error,omitempty"`
	LineNumber int    `json:"lineNumber,omitempty" xml:"lineNumber,omitempty"`
}

type resultXML struct {
	XMLName xml.Name `xml:"result"`
	Results []result `xml:"results"`
}

func getIndexOfField(name string, line []string) int {
	for index, str := range line {
		if strings.Compare(str, name) == 0 {
			return index
		}
	}
	return -1
}

func lineProcessing(strings []string, colA, colB, colC, colD, lineNumber int) result {
	c, err := strconv.Atoi(strings[colC])
	if err != nil {
		return result{"", "colC is not a number", lineNumber}
	}
	d, err := strconv.Atoi(strings[colD])
	if err != nil {
		return result{"", "colD is not a number", lineNumber}
	}
	if c+d < 100 {
		return result{"", "not greater than 100", lineNumber}
	} else {
		return result{Result: strings[colA] + strings[colB], Err: "", LineNumber: lineNumber}
	}

}

func parseFile(fileBytes []byte) []result {

	csvString := string(fileBytes)

	lines := strings.Split(csvString, "\n")
	splittedLines := make([][]string, len(lines))
	for index, modeline := range lines {
		splittedLines[index] = strings.Split(modeline, ";")
	}

	resultArray := make([]result, len(lines))
	r := csv.NewReader(strings.NewReader(csvString))
	r.Comma = ';'
	r.TrailingComma = false
	firstLine := splittedLines[0]

	columnAIndex := getIndexOfField("columnA", firstLine)
	columnBIndex := getIndexOfField("columnB", firstLine)
	columnCIndex := getIndexOfField("columnC", firstLine)
	columnDIndex := getIndexOfField("columnD", firstLine)

	//PROCESSING
	for i := 1; i < len(splittedLines); i++ {

		line := splittedLines[i]
		//if any of the field we need is strictly greater than the line
		// there is a problem with the line
		if !(len(line) < columnAIndex || len(line) < columnBIndex || len(line) < columnCIndex || len(line) < columnDIndex) {
			resultArray[i-1] = lineProcessing(line, columnAIndex, columnBIndex, columnCIndex, columnDIndex, i)
			//printResult(lineProcessing(line, columnAIndex, columnBIndex, columnCIndex, columnDIndex, i), "json")
		} else {
			resultArray[i-1] = result{Result: "", Err: "error parsing line", LineNumber: i}
			//printResult(result{"", "error parsing line", i}, "json")
		}
	}
	return resultArray
}

func printer(outputFormat *string, resultArray []result) string {

	if strings.Compare(*outputFormat, "none") == 0 {
		var strBuilder strings.Builder
		for _, result := range resultArray {
			if len(result.Err) == 0 {
				strBuilder.WriteString(result.Result)
				strBuilder.WriteString("\n")
			}
		}
		return strBuilder.String()
	} else if strings.Compare(*outputFormat, "json") == 0 {

		jsonBytes, err := json.MarshalIndent(resultArray, "", "    ")
		if err != nil {
			log.Fatal("problem with json object")
		}
		return string(jsonBytes)
	} else if strings.Compare(*outputFormat, "xml") == 0 {

		xmlBytes, err := xml.MarshalIndent(resultArray, "", "    ")
		if err != nil {
			log.Fatal("problem with xml object")
		}
		return string(xmlBytes)
	} else {
		return "wrong format"
	}
}

func handleRequest(c *gin.Context) {
	file, err := ioutil.ReadFile(c.Query("csvUri"))
	requestResArray := parseFile(file)
	if err != nil {
		c.JSON(404, "err")
	}
	if strings.Compare(c.Query("format"), "json") == 0 {
		c.JSON(200, requestResArray)
	} else if strings.Compare(c.Query("format"), "xml") == 0 {

		c.XML(200, resultXML{Results: requestResArray})
	}
}

func main() {
	//csvString := readFile("bigfile.csv", ';')

	r := gin.Default()
	r.GET("/filter", handleRequest)
	r.POST("/filter", handleRequest)
	r.Run() // listen and serve on 0.0.0.0:

}
