package main

import "testing"
import "strings"

func TestComputeLine(t *testing.T) {
	result := lineProcessing([]string{"a", "b", "100", "2"}, 0, 1, 2, 3, 1)
	if strings.Compare(result.Result, "ab") != 0 {
		t.Error("wrong result")
	}

	result = lineProcessing([]string{"a", "b", "10", "2"}, 0, 1, 2, 3, 1)
	if strings.Compare(result.Result, "ab") == 0 {
		t.Error("returned when it should not have")
	}

	result = lineProcessing([]string{"a", "b", "100", "a"}, 0, 1, 2, 3, 1)
	if strings.Compare(result.Result, "ab") == 0 {
		t.Error("should return an error and doesnt")
	}
}

func TestGetIndexFound(t *testing.T) {
	index := getIndexOfField("field", []string{"toto", "fielddd", "field"})
	if index != 2 {
		t.Error("problem with get index")
	}
}

func TestGetIndexNotFoundd(t *testing.T) {
	index := getIndexOfField("field", []string{"toto", "fielddd", "fiield"})
	if index != -1 {
		t.Error("problem with get index")
	}
}

func TestParseFile(t *testing.T) {
	fileString := "column;columnA;columnB;columnC;columnD;otherColumn\n" +
		";here is some value 1;columnB;14;11;\n" +
		";here is some value 2;columnB;46;32;\n"
	resultArray := parseFile([]byte(fileString))
	if len(resultArray[0].Err) == 0 {
		t.Error("should have an error because it is not >100")
	}
}
